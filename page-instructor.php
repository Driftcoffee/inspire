<?php
/*
Template Name: Instructor Guides
*/
?>

<?php get_header(); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/flexslider/flexslider.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap.css">
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,600,400,700' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.smooth-scroll.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Francois+One' rel='stylesheet' type='text/css'>
	<?php woo_crumbs(); ?>
	</div><!-- /#top -->
       
    <div id="content">
	<div>   
		<div role="main">
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div class="post page-instructor">

                    <h1 class="title text-center"><?php the_title(); ?></h1>
                    <?php if(get_field("secondary_title")){ ?>
                    <h3 class="text-muted title-h3 text-center"><?php the_field("secondary_title"); ?></h3>
                    <?php } ?>

					<?php
					$slide_1 = get_field("slide_1");
					$slide_2 = get_field("slide_2");
					$slide_3 = get_field("slide_3");
					$slide_4 = get_field("slide_4");
					$slide_5 = get_field("slide_5");
					$enable_slider = get_field("enable_slider");
					if($enable_slider == "Yes"){ ?>
					<div class="flexslider clearfix" style="margin:0px auto;max-width:100%;">
					  <ul class="slides">
					  	<?php if($slide_1){ ?>
					    <li>
					    	<a href="javascript:void(0);">
					      		<img src="<?php echo $slide_1['url'] ?>" />
					    	</a>
					    </li>
					    <?php } ?>
					  	<?php if($slide_2){ ?>
					    <li>
					    	<a href="javascript:void(0);">
					      		<img src="<?php echo $slide_2['url'] ?>" />
					    	</a>
					    </li>
					    <?php } ?>
					  	<?php if($slide_3){ ?>
					    <li>
					    	<a href="javascript:void(0);">
					    		<img src="<?php echo $slide_3['url'] ?>" />
					    	</a>
					    </li>
					    <?php } ?>
					  	<?php if($slide_4){ ?>
					    <li>
					    	<a href="javascript:void(0);">
					    		<img src="<?php echo $slide_4['url'] ?>" />
					    	</a>
					    </li>
					    <?php } ?>
					  	<?php if($slide_5){ ?>
					    <li>
					    	<a href="javascript:void(0);">
					    		<img src="<?php echo $slide_5['url'] ?>" />
					    	</a>
					    </li>
					    <?php } ?>
					  </ul>
					</div>
					<?php } ?>

                    <div class="entry entry-clear container">
                    	<div class="text-center" style="margin-top:30px;">
                    		<h3 class="fsize-24">WE ARE COMMITED TO OFFERING THE BEST SURFING EXPERIENCE</h3>
                    		<p style="padding:0px 165px;">Our curated collection of material is for you to excel as a Carolina School of Surf Instructor. Take this knowledge and apply it to each activity you lead.</p>
                    	</div>

	                	<?php //the_content(); ?>
						<div id="tabs">

							<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
								<li class="ui-state-default ui-corner-top ui-tabs-active" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#surfing" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">SURFING</a></li>
								<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#sup" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">SUP</a></li>
								<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false"><a href="#safety" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">SAFETY</a></li>
								<?php
									if (get_field("enable_custom_tab_1") == "Yes"){
								?>
									<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false"><a href="#customtab1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3"><?php the_field("custom_tab_1_title"); ?></a></li>
								<?php } ?>
								<?php
									if (get_field("enable_custom_tab_2") == "Yes"){
								?>
									<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false"><a href="#customtab1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3"><?php the_field("custom_tab_2_title"); ?></a></li>
								<?php } ?>
							</ul>


							<div id="tabs-1">
								<h3 id="surfing" class="text-center fsize-24" style="margin:80px 0px 30px 0px;">Surfing</h3>
								<?php if(get_field("pop_up_textarea_1") || get_field("pop_up_video_1") || get_field("pop_up_textarea_2") || get_field("pop_up_video_2")){ ?>
									<h4 class="text-green">Pop Up</h4>
								<?php } ?>
								<div class="row">
									<?php if(get_field("pop_up_textarea_1")){ ?>
										<div class="col-xs-6"><?php the_field("pop_up_textarea_1"); ?></div>
									<?php }
									if(get_field("pop_up_video_1")){ ?>
										<div class="col-xs-6"><?php the_field("pop_up_video_1"); ?></div>
									<?php } ?>
								</div>
								<div class="row">
									<?php if(get_field("pop_up_textarea_2")){ ?>
										<div class="col-xs-6"><?php the_field("pop_up_textarea_2"); ?></div>
									<?php }
									if(get_field("pop_up_video_2")){ ?>
										<div class="col-xs-6"><?php the_field("pop_up_video_2"); ?></div>
									<?php } ?>
								</div>
								<?php if(get_field("riding_waves_textarea_1") || get_field("riding_waves_video")){ ?>
									<h4 class="text-green">Riding Waves</h4>
								<?php } ?>
								<div class="row">
									<?php if(get_field("riding_waves_textarea_1")){ ?>
										<div class="col-xs-6"><?php the_field("riding_waves_textarea_1"); ?></div>
									<?php }
									if(get_field("riding_waves_video")){ ?>
										<div class="col-xs-6"><?php the_field("riding_waves_video"); ?></div>
									<?php } ?>
								</div>
								<?php if(get_field("riding_waves_textarea_1") || get_field("reading_waves_video") || get_field("reading_waves_textarea_2")){ ?>
									<h4 class="text-green">Reading Waves</h4>
								<?php } ?>
								<div class="row">
									<?php if(get_field("riding_waves_textarea_1")){ ?>
										<div class="col-xs-6"><?php the_field("reading_waves_textarea_1"); ?></div>
									<?php }
									if(get_field("reading_waves_video")){ ?>
										<div class="col-xs-6"><?php the_field("reading_waves_video"); ?></div>
									<?php } ?>
								</div>
								<div class="row">
									<?php if(get_field("reading_waves_textarea_2")){ ?>
										<div class="col-xs-12"><?php the_field("reading_waves_textarea_2"); ?></div>
									<?php } ?>
								</div>

							<?php if (get_field("custom_surfing_1_subtitle")){ ?>

								<h4 class="text-green"><?php the_field("custom_surfing_1_subtitle"); ?></h4>
								<div class="row">
									<div class="col-xs-<?php the_field('custom_surfing_content_1_columns'); ?>"><?php the_field("custom_surfing_content_1"); ?></div>
									<div class="col-xs-<?php the_field('custom_surfing_content_2_columns'); ?>"><?php the_field("custom_surfing_content_2"); ?></div>
								</div>
							<?php } ?>

							<?php if (get_field("custom_surfing_2_subtitle")){ ?>

								<h4 class="text-green"><?php the_field("custom_surfing_2_subtitle"); ?></h4>
								<div class="row">
									<div class="col-xs-<?php the_field('custom_surfing_2_content_1_columns'); ?>"><?php the_field("custom_surfing_2_content_1"); ?></div>
									<div class="col-xs-<?php the_field('custom_surfing_2_content_2_columns'); ?>"><?php the_field("custom_surfing_2_content_2"); ?></div>
								</div>
							<?php } ?>

							</div>

							<div id="tabs-2">
								<h3 id="sup" class="text-center fsize-24" style="margin:80px 0px 30px 0px;">SUP</h3>
								<?php if(get_field("techniques_textarea_1") || get_field("techniques_video_1") || get_field("techniques_textarea_2") || get_field("techniques_video_2") || get_field("techniques_textarea_3")){ ?>
								<h4 class="text-green">Techniques</h4>
								<?php } ?>
								<div class="row">
									<?php if(get_field("techniques_textarea_1")){ ?>
										<div class="col-xs-6"><?php the_field("techniques_textarea_1"); ?></div>
									<?php }
									if(get_field("techniques_video_1")){ ?>
										<div class="col-xs-6"><?php the_field("techniques_video_1"); ?></div>
									<?php } ?>
								</div>
								<div class="row">
									<?php if(get_field("techniques_textarea_2")){ ?>
										<div class="col-xs-6"><?php the_field("techniques_textarea_2"); ?></div>
									<?php }
									if(get_field("techniques_video_2")){ ?>
										<div class="col-xs-6"><?php the_field("techniques_video_2"); ?></div>
									<?php } ?>
								</div>
								<div class="row">
									<?php if(get_field("techniques_textarea_3")){ ?>
										<div class="col-xs-12"><?php the_field("techniques_textarea_3"); ?></div>
									<?php } ?>
								</div>

							<?php if (get_field("custom_sup_1_subtitle")){ ?>

								<h4 class="text-green"><?php the_field("custom_sup_1_subtitle"); ?></h4>
								<div class="row">
									<div class="col-xs-<?php the_field('custom_sup_content_1_columns'); ?>"><?php the_field("custom_sup_content_1"); ?></div>
									<div class="col-xs-<?php the_field('custom_sup_content_2_columns'); ?>"><?php the_field("custom_sup_content_2"); ?></div>
								</div>
							<?php } ?>

							<?php if (get_field("custom_sup_2_subtitle")){ ?>

								<h4 class="text-green"><?php the_field("custom_sup_2_subtitle"); ?></h4>
								<div class="row">
									<div class="col-xs-<?php the_field('custom_sup_2_content_1_columns'); ?>"><?php the_field("custom_sup_2_content_1"); ?></div>
									<div class="col-xs-<?php the_field('custom_sup_2_content_2_columns'); ?>"><?php the_field("custom_sup_2_content_2"); ?></div>
								</div>
							<?php } ?>

							</div>

							<div id="tabs-3">

								<h3 id="safety" class="text-center fsize-24" style="margin:80px 0px 30px 0px;">SAFETY</h3>
								<?php if(get_field("rip_currents_textarea_1") || get_field("rip_currents_video_1") || get_field("rip_currents_textarea_2") || get_field("rip_currents_video_2") || get_field("rip_currents_textarea_3")){ ?>
									<h4 class="text-green">Rip Currents</h4>
								<?php } ?>
								<div class="row">
									<?php if(get_field("rip_currents_textarea_1")){ ?>
										<div class="col-xs-6"><?php the_field("rip_currents_textarea_1"); ?></div>
									<?php }
									if(get_field("rip_currents_video_1")){ ?>
										<div class="col-xs-6"><?php the_field("rip_currents_video_1"); ?></div>
									<?php } ?>
								</div>
								<div class="row">
									<?php if(get_field("rip_currents_textarea_2")){ ?>
										<div class="col-xs-6"><?php the_field("rip_currents_textarea_2"); ?></div>
									<?php }
									if(get_field("rip_currents_video_2")){ ?>
										<div class="col-xs-6"><?php the_field("rip_currents_video_2"); ?></div>
									<?php } ?>
								</div>
								<div class="row">
									<?php if(get_field("rip_currents_textarea_3")){ ?>
										<div class="col-xs-12"><?php the_field("rip_currents_textarea_3"); ?></div>
									<?php } ?>
								</div>

							<?php if (get_field("custom_safety_1_subtitle")){ ?>

								<h4 class="text-green"><?php the_field("custom_safety_1_subtitle"); ?></h4>
								<div class="row">
									<div class="col-xs-<?php the_field('custom_safety_content_1_columns'); ?>"><?php the_field("custom_safety_content_1"); ?></div>
									<div class="col-xs-<?php the_field('custom_safety_content_2_columns'); ?>"><?php the_field("custom_safety_content_2"); ?></div>
								</div>
							<?php } ?>

							<?php if (get_field("custom_safety_2_subtitle")){ ?>

								<h4 class="text-green"><?php the_field("custom_safety_2_subtitle"); ?></h4>
								<div class="row">
									<div class="col-xs-<?php the_field('custom_safety_2_content_1_columns'); ?>"><?php the_field("custom_safety_2_content_1"); ?></div>
									<div class="col-xs-<?php the_field('custom_safety_2_content_2_columns'); ?>"><?php the_field("custom_safety_2_content_2"); ?></div>
								</div>
							<?php } ?>

							</div>

							<?php if (get_field("enable_custom_tab_1") == "Yes"){ ?>
							<div id="tabs-4">

								<h3 id="customtab1" class="text-center fsize-24" style="margin:80px 0px 30px 0px;"><?php the_field("custom_tab_1_title"); ?></h3>
								<h4 class="text-green"><?php the_field("custom_tab_1_subtitle"); ?></h4>
								<div class="row">
									<div class="col-xs-<?php the_field('custom_tab_content_1_columns'); ?>"><?php the_field("custom_tab_content_1"); ?></div>
									<div class="col-xs-<?php the_field('custom_tab_content_2_columns'); ?>"><?php the_field("custom_tab_content_2"); ?></div>
									<div class="col-xs-<?php the_field('custom_tab_content_3_columns'); ?>"><?php the_field("custom_tab_content_3"); ?></div>
								</div>

							</div>
							<?php } ?>

	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                   
                   <?php $comm = get_option('woo_comments'); if ( 'open' == $post->comment_status && ($comm == "page" || $comm == "both") ) : ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->
		
	</div><!-- /#col-full -->
    </div><!-- /#content -->

<!-- FlexSlider -->
<script defer src="<?php bloginfo('template_url');?>/flexslider/jquery.flexslider-min.js"></script>


<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php bloginfo('template_url');?>/flexslider/demo/js/shCore.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/flexslider/demo/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/flexslider/demo/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php bloginfo('template_url');?>/flexslider/demo/js/jquery.easing.js"></script>

<script src="<?php bloginfo('template_url');?>/flexslider/demo/js/jquery.mousewheel.js"></script>

<script type="text/javascript">
// Can also be used with $(document).ready()
$(document).ready(function() {

  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: false,
    reverse: false,
    animationLoop: false
  });

});
</script>

<script type="text/javascript">


	$('.page-instructor a').smoothScroll();

</script>

<?php get_footer(); ?>