<?php
/*
Template Name: Surf Lesson
*/
?>

<?php get_header(); ?>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<?php woo_crumbs(); ?>
	</div><!-- /#top -->
       
    <div id="content">
	<div>   
		<div role="main">
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div class="post page-surflesson">

                    <h1 class="title text-center"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
                    <?php if(get_field("secondary_title")){ ?>
                    <h3 class="text-muted title-h3 text-center"><?php the_field("secondary_title"); ?></h3>
                    <?php } ?>
					<div class="container container-page-surflesson-tabs clearfix">
						<div class="tab tab1 tab-bg">
							<h2 class="font-tillium"><?php the_field("tab_1_title"); ?></h2>
							<small>LEARN MORE</small>
                    	</div>
						<div class="tab tab2">
							<h2 class="font-tillium"><?php the_field("tab_2_title"); ?></h2>
							<small>LEARN MORE</small>
						</div>
					</div>

                    <div class="entry entry-clear">
	                	<?php //the_content(); ?>
						<div id="tabs">
							<ul>
								<li><a href="#tabs-1">PRIVATE</a></li>
								<li><a href="#tabs-2">GROUP</a></li>
								<li><a href="#tabs-3">PRIVATE</a></li>
								<li><a href="#tabs-4">GROUP</a></li>
							</ul>
							<div id="tabs-1" class="container">
								<?php the_field("tab_1_private_lesson_content"); ?>
							</div>
							<div id="tabs-2" class="container">
								<?php the_field("tab_1_group_lesson_content"); ?>
							</div>
							<div id="tabs-3" class="container">
								<?php the_field("tab_2_private_lesson_content"); ?>
							</div>
							<div id="tabs-4" class="container">
								<?php the_field("tab_2_group_lesson_content"); ?>
							</div>
						</div>

	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                   
                   <?php $comm = get_option('woo_comments'); if ( 'open' == $post->comment_status && ($comm == "page" || $comm == "both") ) : ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->
		
	</div><!-- /#col-full -->
    </div><!-- /#content -->

<script type="text/javascript">
	$(function() {
		$( "#tabs" ).tabs({
			collapsible: true,
			active: 0,
			show: { 
				effect: "fade"
			}
		});
	});
	jQuery(document).ready(function(){
		jQuery("#ui-id-1,#ui-id-2").click(function(){
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab1").addClass("tab-bg");
		});
		jQuery(".tab1").click(function(){
			$("#tabs").tabs("option", "active", 0);
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab1").addClass("tab-bg");
		});
		jQuery("#ui-id-3,#ui-id-4").click(function(){
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab2").addClass("tab-bg");
		});
		jQuery(".tab2").click(function(){
			$("#tabs").tabs("option", "active", 2);
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab2").addClass("tab-bg");
		});
	});
</script>



<?php get_footer(); ?>