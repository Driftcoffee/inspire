<?php
/*
Template Name: Surf Camp
*/
?>

<?php get_header(); ?>
	<?php woo_crumbs(); ?>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	</div><!-- /#top -->
      
    <div id="content">
	<div class="col-full">   
		<div id="main" class="fullwidth">
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div class="post page-surfcamp">

                    <h1 class="title text-center"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
                    <?php if(get_field("secondary_title")){ ?>
                    <h3 class="text-muted title-h3 text-center"><?php the_field("secondary_title"); ?></h3>
                    <?php } ?>
					<div class="container-page-surfcamp-tabs clearfix">
						<div class="tab tab1 tab-bg">
							<h2 class="font-tillium"><?php the_field("tab_1_title"); ?></h2>
							<small>LEARN MORE</small>
                    	</div>
						<div class="tab tab2">
							<h2 class="font-tillium"><?php the_field("tab_2_title"); ?></h2>
							<small>LEARN MORE</small>
						</div>
					</div>

                    <div class="entry entry-clear">
	                	<?php //the_content(); ?>
						<div class="tab-content tab-content-1">
							<?php the_field("tab_1_content"); ?>
						</div>
	                	<div class="tab-content tab-content-2">
	                		<?php the_field("tab_2_content"); ?>
	                	</div>
	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                   
                   <?php $comm = get_option('woo_comments'); if ( 'open' == $post->comment_status && ($comm == "page" || $comm == "both") ) : ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->
		
	</div><!-- /#col-full -->
    </div><!-- /#content -->
	
<script type="text/javascript">
	jQuery(".tab1").click(function(){
		jQuery(".tab").removeClass("tab-bg");
		jQuery(this).addClass("tab-bg");
		jQuery(".tab-content-2").hide()
		jQuery(".tab-content-1").slideDown();
	});
	jQuery(".tab2").click(function(){
		jQuery(".tab").removeClass("tab-bg");
		jQuery(this).addClass("tab-bg");
		jQuery(".tab-content-1").hide();
		jQuery(".tab-content-2").slideDown();
	});
</script>

<?php get_footer(); ?>