<?php
add_action( 'admin_init', 'carolinasurt_general' );

function carolinasurt_general() {
		register_setting( 'carolinasurf_generals', 'sidevideo' );
		register_setting( 'carolinasurf_generals', 'footer_background' );
		register_setting( 'carolinasurf_generals', 'custom_css' );
		register_setting( 'carolinasurf_generals', 'footer_menu' );
		register_setting( 'carolinasurf_generals', 'footer_icons' );
		register_setting( 'carolinasurf_generals', 'footer_copyright' );
		register_setting( 'carolinasurf_generals', 'contact_form' );
		register_setting( 'carolinasurf_generals', 'header_background' );
		register_setting( 'carolinasurf_generals', 'news_background' );
}


function carolinasurf_video(){ ?>
	<div class="wrap">
		<div id="icon-options-general" class="icon32"><br></div>
		<h2>General Options</h2>
		
		<form method="post" action="options.php">
		<?php settings_fields('carolinasurf_generals');?>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>Sidebar Video</h2>
				<textarea style="width:100%;" cols="100" rows="5" name="sidevideo" id="sidevideo"><?php if (get_option('sidevideo') != ""){ echo get_option('sidevideo'); } ?></textarea>
				<p>Put your video iframe here.</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>Footer Background</h2>
				<input style="width:100%;" type="text" name="footer_background" id="footer_background" value="<?php if (get_option('footer_background') != ""){ echo get_option('footer_background'); } ?>">
				<h2>Footer Content</h2>
				<textarea style="width:100%;" cols="100" rows="5" name="footer_menu" id="footer_menu"><?php if (get_option('footer_menu') != ""){ echo get_option('footer_menu'); } ?></textarea>
				<p>Footer HTML Code.</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>Footer Icons</h2>
				<textarea style="width:100%;" cols="100" rows="5" name="footer_icons" id="footer_icons"><?php if (get_option('footer_icons') != ""){ echo get_option('footer_icons'); } ?></textarea>
				<p>Footer Icons.</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>Footer Copyright</h2>
				<textarea style="width:100%;" cols="100" rows="5" name="footer_copyright" id="footer_copyright"><?php if (get_option('footer_copyright') != ""){ echo get_option('footer_copyright'); } ?></textarea>
				<p>Footer Copyright.</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>Contact Form Shortcode</h2>
				<textarea style="width:100%;" cols="100" rows="2" name="contact_form" id="contact_form"><?php if (get_option('contact_form') != ""){ echo get_option('contact_form'); } ?></textarea>
				<p>Footer Contact Form Shortcode.</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>Header Background</h2>
				<input style="width:100%;" type="text" name="header_background" id="header_background" value="<?php if (get_option('header_background') != ""){ echo get_option('header_background'); } ?>">
				<p>Header Background URL.</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>News Background</h2>
				<input style="width:100%;" type="text" name="news_background" id="news_background" value="<?php if (get_option('news_background') != ""){ echo get_option('news_background'); } ?>">
				<p>News Background URL.</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<div style="margin:20px 0;padding:20px;background:none;">
			<div style="border:1px solid #333;padding:20px;margin:20px 0;background:#f1f1f1;">
				<h2>Custom CSS/Script</h2>
				<textarea style="width:100%;" cols="100" rows="5" name="custom_css" id="custom_css"><?php if (get_option('custom_css') != ""){ echo get_option('custom_css'); } ?></textarea>
				<p>Custom CSS/Script</p>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
			</p>
			</div>
		</div>
		<p class="submit">
		<input type="submit" class="button-primary" value="<?php _e('Save') ?>" />
		</p>

		</form>	
	
	
	</div>
<?php } ?>