<?php
/*
Template Name: Surf Camp Full Width
*/
?>

<?php get_header(); ?>
	<?php woo_crumbs(); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/flexslider/flexslider.css">
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,600,400,700' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.smooth-scroll.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Francois+One' rel='stylesheet' type='text/css'>
	</div><!-- /#top -->
      
    <div id="content">
	<div>   
		<div role="main">
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div class="post page-surfcamp">

                    <h1 class="title text-center"><?php the_title(); ?></h1>
                    <?php if(get_field("secondary_title")){ ?>
                    <h3 class="text-muted title-h3 text-center"><?php the_field("secondary_title"); ?></h3>
                    <?php } ?>
					<div class="container-page-surfcamp-tabs clearfix container">
						<div class="tab tab1 tab-bg">
							<h2 class="font-tillium"><?php the_field("tab_1_title"); ?></h2>
							<small>LEARN MORE</small>
                    	</div>
						<div class="tab tab2">
							<h2 class="font-tillium"><?php the_field("tab_2_title"); ?></h2>
							<small>LEARN MORE</small>
						</div>
					</div>

                    <div class="entry entry-clear">
	                	<?php //the_content(); ?>
						<div class="tab-content tab-content-1">
							<?php
							$slide_1 = get_field("slide_1");
							$slide_2 = get_field("slide_2");
							$slide_3 = get_field("slide_3");
							$slide_4 = get_field("slide_4");
							$slide_5 = get_field("slide_5");
							$enable_slider = get_field("enable_slider");
							if($enable_slider == "Yes"){ ?>
							<div class="flexslider clearfix" style="margin:0px auto;max-width:100%;">
							  <ul class="slides">
							  	<?php if($slide_1){ ?>
							    <li>
							    	<a href="#calendar">
							      		<img src="<?php echo $slide_1['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_2){ ?>
							    <li>
							    	<a href="#calendar">
							      		<img src="<?php echo $slide_2['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_3){ ?>
							    <li>
							    	<a href="#calendar">
							    		<img src="<?php echo $slide_3['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_4){ ?>
							    <li>
							    	<a href="#calendar">
							    		<img src="<?php echo $slide_4['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_5){ ?>
							    <li>
							    	<a href="#calendar">
							    		<img src="<?php echo $slide_5['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  </ul>
							</div>
							<?php } ?>

							<div class="container" <?php if($enable_slider != "Yes"){ echo "style=\"margin-top:36px;\""; } ?>>
								<h3 class="text-center"><?php the_field("content_title"); ?></h3>
									<p class="text-center" style="margin-bottom: 10px;">
										<?php the_field("content_title_p"); ?>
									</p>
									<p class="text-center">
										<a class="btn-blue center-block btn-lg font-francoise" href="#calendar">SIGN UP</a>
									</p>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_1"); ?>
										</div>
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_2"); ?>
										</div>
									</div>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_3"); ?>
										</div>
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_4"); ?>
										</div>
									</div>
							</div>


							<div id="map-canvas"></div>
							<div class="container clearfix" style="margin-top:30px">
								<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
									<div style="width:50%;float:left;">
										<h4 class="text-green" style="font-size: 24px;text-align: right;padding-top: 10px;color: #333;">SURF CAMP LOCATION:</h4>
									</div>
									<div style="width:50%;float:left;">
										<h4 class="text-green" style="text-align: left;box-sizing:border-box;padding-left:20px;line-height: 22px;">44 East 1st Street<br/>
										<span style="font-size:22px;">Ocean Isle Beach, NC</span></h4>
									</div>
								</div>
								<p class="text-center"><?php the_field("location_description"); ?></p>
								<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Covered Gazebo</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Showers</p>
									</div>
								</div>
								<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Free Parking Available</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Public Restrooms</p>
									</div>
								</div>
								<h4 class="text-center text-green" style="font-size: 24px;color: #333;margin-top: 20px;">What's Included?</h4>
								<div class="clearfix text-center row" style="box-sizing:border-box;margin-top:30px;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted">8', 9' or Performance Surfboard</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted">Water Cooler, Umbrella and Table</p>
									</div>
								</div>
								<div class="clearfix text-center row" style="box-sizing:border-box;margin-bottom:20px;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted">Prizes</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted">"Surftificate" of Completion</p>
									</div>
								</div>
							</div>
							<div style="background-color: rgb(236, 235, 231);background-image: url('/wp-content/themes/inspire/images/fruute-special-cookie-cloud.jpg');background-position: center 0px;padding:40px 0px 0px 0px;background-repeat: repeat-x;">
							  	<div class="container">
									<h3 style="font-size:24px;color:#333;padding-left:3%;"><?php the_field("cloud_container_title_1"); ?></h3>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width: 50%;float: left;padding-right: 35px;box-sizing: border-box;padding-left: 3%;">
											<p style="text-align: justify;"><?php the_field("cloud_container_content_1"); ?></p>
										</div>
										<div style="width:40%;float:left;">
											<p class="text-muted"><?php the_field("iframe_video"); ?></p>
										</div>
									</div>
									<h3 style="font-size:24px;color:#333;padding-left:3%;"><?php the_field("cloud_container_title_2"); ?></h3>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width: 50%;float: left;padding-right: 35px;box-sizing: border-box;padding-left: 3%;">
											<p style="text-align: justify;">
											<p style="text-align: justify;">
												<?php the_field("cloud_container_content_2"); ?>
											</p>
										</div>
										<div style="width:40%;float:left;">
											<img alt="surfcamp" src="<?php bloginfo('template_url') ?>/images/surfcamp.jpg" />
										</div>
									</div>
								</div>
							</div>
							<div class="container clearfix">
								<h3 id="calendar" style="font-size:24px;color:#333;margin-top:30px;" class="text-center">INTERACTIVE BOOKING CALENDAR</h3>
								<div style="margin-left:3%;">
								<?php the_field("calendar_script"); ?>
								</div>
								<h3 style="font-size:24px;color:#333;margin: 20px 0px 0px 3%;"><?php the_field("policy_title"); ?></h3>
								<p style="margin-left:3%;;">
									<?php the_field("policy_content"); ?>  
								</p>
								<div class="clearfix text-center" style="margin: 0px auto 10px auto;width:590px;">
									<div style="width:33%;box-sizing:border-box;float:left;">
										<a class="btn-white" style="padding:5px 67px;" href="mailto:carolinaschoolofsurf@gmail.com">EMAIL US</a>
									</div>
									<div class="text-muted font-tillium" style="width:33%;box-sizing:border-box;float:left;">
										HOW CAN WE HELP
									</div>
									<div style="width:33%;box-sizing:border-box;float:left;">
										<a style="padding: 5px 50px;" class="btn-white" href="javascript:void(0)">(910)713-9283</a>
									</div>
								</div>
							</div>

							<?php the_field("tab_1_content"); ?>

						</div>

	                	<div class="tab-content tab-content-2">
	                		
							<?php
							$slide_1 = get_field("slide_1_(copy)");
							$slide_2 = get_field("slide_2_(copy)");
							$slide_3 = get_field("slide_3_(copy)");
							$slide_4 = get_field("slide_4_(copy)");
							$slide_5 = get_field("slide_5_(copy)");
							$enable_slider = get_field("enable_slider_(copy)");
							if($enable_slider == "Yes"){ ?>
							<div class="flexslider2 clearfix" style="margin:0px auto;max-width:100%;">
							  <ul class="slides">
							  	<?php if($slide_1){ ?>
							    <li>
							    	<a href="#calendar2">
							      		<img src="<?php echo $slide_1['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_2){ ?>
							    <li>
							    	<a href="#calendar2">
							      		<img src="<?php echo $slide_2['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_3){ ?>
							    <li>
							    	<a href="#calendar2">
							    		<img src="<?php echo $slide_3['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_4){ ?>
							    <li>
							    	<a href="#calendar2">
							    		<img src="<?php echo $slide_4['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  	<?php if($slide_5){ ?>
							    <li>
							    	<a href="#calendar2">
							    		<img src="<?php echo $slide_5['url'] ?>" />
							    	</a>
							    </li>
							    <?php } ?>
							  </ul>
							</div>
							<?php } ?>

							<div class="container" <?php if($enable_slider != "Yes"){ echo "style=\"margin-top:36px;\""; } ?>>
								<h3 class="text-center"><?php the_field("content_title_(copy)"); ?></h3>
									<p class="text-center" style="margin-bottom: 10px;">
										<?php the_field("content_title_p_(copy)"); ?>
									</p>
									<p class="text-center">
										<a class="btn-blue center-block btn-lg font-francoise" href="#calendar2">SIGN UP</a>
									</p>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_1_(copy)"); ?>
										</div>
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_2_(copy)"); ?>
										</div>
									</div>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_3_(copy)"); ?>
										</div>
										<div style="width:50%;float:left;">
											<?php the_field("green_text_box_4_(copy)"); ?>
										</div>
									</div>
							</div>


							<div id="map-canvas2"></div>
							<div class="container clearfix" style="margin-top:30px">
								<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
									<div style="width:50%;float:left;">
										<h4 class="text-green" style="font-size: 24px;text-align: right;padding-top: 10px;color: #333;">SURF CAMP LOCATION:</h4>
									</div>
									<div style="width:50%;float:left;">
										<h4 class="text-green" style="text-align: left;box-sizing:border-box;padding-left:20px;line-height: 22px;">118 Ocean Blvd E<br/>
										<span style="font-size:22px;">&nbsp;Holden Beach, NC</span></h4>
									</div>
								</div>
								<p class="text-center"><?php the_field("location_description_(copy)"); ?></p>
								<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Covered Gazebo</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Showers</p>
									</div>
								</div>
								<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Free Parking Available</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted font-tillium">Public Restrooms</p>
									</div>
								</div>
								<h4 class="text-center text-green" style="font-size: 24px;color: #333;margin-top: 20px;">What's Included?</h4>
								<div class="clearfix text-center row" style="box-sizing:border-box;margin-top:30px;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted">8', 9' or Performance Surfboard</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted">Water Cooler, Umbrella and Table</p>
									</div>
								</div>
								<div class="clearfix text-center row" style="box-sizing:border-box;margin-bottom:20px;padding: 0px 200px;">
									<div style="width:50%;float:left;">
										<p class="text-muted">Prizes</p>
									</div>
									<div style="width:50%;float:left;">
										<p class="text-muted">"Surftificate" of Completion</p>
									</div>
								</div>
							</div>
							<div style="background-color: rgb(236, 235, 231);background-image: url('/wp-content/themes/inspire/images/fruute-special-cookie-cloud.jpg');background-position: center 0px;padding:40px 0px 0px 0px;background-repeat: repeat-x;">
							  	<div class="container">
									<h3 style="font-size:24px;color:#333;padding-left:3%;"><?php the_field("cloud_container_title_1_(copy)"); ?></h3>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width: 50%;float: left;padding-right: 35px;box-sizing: border-box;padding-left: 3%;">
											<p style="text-align: justify;"><?php the_field("cloud_container_content_1_(copy)"); ?></p>
										</div>
										<div style="width:40%;float:left;">
											<p class="text-muted"><?php the_field("iframe_video_(copy)"); ?></p>
										</div>
									</div>
									<h3 style="font-size:24px;color:#333;padding-left:3%;"><?php the_field("cloud_container_title_2_(copy)"); ?></h3>
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width: 50%;float: left;padding-right: 35px;box-sizing: border-box;padding-left: 3%;">
											<p style="text-align: justify;">
											<p style="text-align: justify;">
												<?php the_field("cloud_container_content_2_(copy)"); ?>
											</p>
										</div>
										<div style="width:40%;float:left;">
											<img alt="surfcamp" src="<?php bloginfo('template_url') ?>/images/surfcamp.jpg" />
										</div>
									</div>
								</div>
							</div>
							<div class="container clearfix">
								<h3 id="calendar2" style="font-size:24px;color:#333;margin-top:30px;" class="text-center">INTERACTIVE BOOKING CALENDAR</h3>
								<div style="margin-left:3%;">
									<?php the_field("calendar_script_(copy)"); ?>
								</div>
								<h3 style="font-size:24px;color:#333;margin: 20px 0px 0px 3%;"><?php the_field("policy_title_(copy)"); ?></h3>
								<p style="margin-left:3%;;">
									<?php the_field("policy_content_(copy)"); ?>  
								</p>
								<div class="clearfix text-center" style="margin: 0px auto 10px auto;width:590px;">
									<div style="width:33%;box-sizing:border-box;float:left;">
										<a class="btn-white" style="padding:5px 67px;" href="mailto:carolinaschoolofsurf@gmail.com">EMAIL US</a>
									</div>
									<div class="text-muted font-tillium" style="width:33%;box-sizing:border-box;float:left;">
										HOW CAN WE HELP
									</div>
									<div style="width:33%;box-sizing:border-box;float:left;">
										<a style="padding: 5px 50px;" class="btn-white" href="javascript:void(0)">(910)713-9283</a>
									</div>
								</div>
							</div>

	                		<?php the_field("tab_2_content"); ?>
	                		
	                	</div>
	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                   
                   <?php $comm = get_option('woo_comments'); if ( 'open' == $post->comment_status && ($comm == "page" || $comm == "both") ) : ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->
		
	</div><!-- /#col-full -->
    </div><!-- /#content -->
	
<!-- FlexSlider -->
<script defer src="<?php bloginfo('template_url');?>/flexslider/jquery.flexslider-min.js"></script>


<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php bloginfo('template_url');?>/flexslider/demo/js/shCore.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/flexslider/demo/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/flexslider/demo/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php bloginfo('template_url');?>/flexslider/demo/js/jquery.easing.js"></script>

<script src="<?php bloginfo('template_url');?>/flexslider/demo/js/jquery.mousewheel.js"></script>

<script type="text/javascript">
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.page-surfcamp a').smoothScroll();
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: false,
    reverse: false,
    animationLoop: false
  });
  $('.flexslider2').flexslider({
    animation: "slide",
    controlNav: false,
    reverse: false,
    animationLoop: false
  });
});
</script>

<style type="text/css">
  #map-canvas,#map-canvas2{
    margin: 0px auto;
    padding: 0;
    height: 350px;
    width: 100%;
  }
  @media(max-width: 1200px){
  	#map-canvas,#map-canvas2{
  		width: 97%;
  	}
  }
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
  // Enable the visual refresh
  google.maps.visualRefresh = true;
  var map;
  var map2;
  function initialize() {
    var mapOptions = {
      zoom: 15,
      center: new google.maps.LatLng(33.888252, -78.432686),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      //disableDefaultUI: true,
      scrollwheel: false
    };
    var mapOptions2 = {
      zoom: 15,
      center: new google.maps.LatLng(33.913814, -78.265631),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      //disableDefaultUI: true,
      scrollwheel: false
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
    map2 = new google.maps.Map(document.getElementById('map-canvas2'),mapOptions2);
    //Loading Markers
    var position = new Array();
    var position2 = new Array();
    position[0] = new google.maps.LatLng(33.888252, -78.432686);
    position2[0] = new google.maps.LatLng(33.913814, -78.265631);
    loadMarkers(position,0);
    loadMarkers2(position2,0);
    function loadMarkers(position,i){
      var marker = new google.maps.Marker({
        position: position[i],
        map: map,
        animation: google.maps.Animation.DROP,
        //icon: image
        //title: 'Uluru (Ayers Rock)'
      });
      marker.setTitle((i + 1).toString());
      //attachSecretMessage(marker, i);
      if (i <= position.length)
        return loadMarkers(position,i+1);
    }
    function loadMarkers2(position,i){
      var marker = new google.maps.Marker({
        position: position[i],
        map: map2,
        animation: google.maps.Animation.DROP,
        //icon: image
        //title: 'Uluru (Ayers Rock)'
      });
      marker.setTitle((i + 1).toString());
      //attachSecretMessage(marker, i);
      if (i <= position.length)
        return loadMarkers2(position,i+1);
    }
    function attachSecretMessage(marker, num) {
      var message = Array('<b>Carolina School of Surf</b>');
      infowindow = new google.maps.InfoWindow({
        //content: message[num],
        maxWidth: 200
      });
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(message[num]);
        infowindow.open(marker.get('map'), marker);
        //toggleBounce(marker);
      });
      google.maps.event.trigger(marker, 'click');
    }
    //End Loading Markers
  }
  //End Initialize
  //ClickToggle Animation
  function toggleBounce(marker) {
    if (marker.getAnimation() != null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
  }
  //End ClickToggle Animation
  google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
	jQuery(".tab1").click(function(){
		jQuery(".tab").removeClass("tab-bg");
		jQuery(this).addClass("tab-bg");
		jQuery(".tab-content-2").hide()
		jQuery(".tab-content-1").fadeIn("slow");
		initialize();
	});
	jQuery(".tab2").click(function(){
		jQuery(".tab").removeClass("tab-bg");
		jQuery(this).addClass("tab-bg");
		jQuery(".tab-content-1").hide();
		jQuery(".tab-content-2").fadeIn("slow");
		initialize();
	});

	var hash = window.location.hash;
	if( hash == "#1" ){
		jQuery(".tab1").trigger("click");
	}else if ( hash == "#2" ){
		jQuery(".tab2").trigger("click");
	}

</script>

<?php get_footer(); ?>