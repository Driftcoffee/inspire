<?php
/*
Template Name: Surf Lesson Full Width
*/
?>

<?php get_header(); ?>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,600,400,700' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.smooth-scroll.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Francois+One' rel='stylesheet' type='text/css'>
	<?php woo_crumbs(); ?>
	</div><!-- /#top -->
       
    <div id="content">
	<div>   
		<div role="main">
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div class="post page-surflesson">

                    <h1 class="title text-center"><?php the_title(); ?></h1>
                    <?php if(get_field("secondary_title")){ ?>
                    <h3 class="text-muted title-h3 text-center"><?php the_field("secondary_title"); ?></h3>
                    <?php } ?>
					<div class="container-page-surflesson-tabs clearfix container">
						<div class="tab tab1 tab-bg">
							<h2 class="font-tillium"><?php the_field("tab_1_title"); ?></h2>
							<small>LEARN MORE</small>
                    	</div>
						<div class="tab tab2">
							<h2 class="font-tillium"><?php the_field("tab_2_title"); ?></h2>
							<small>LEARN MORE</small>
						</div>
					</div>

                    <div class="entry entry-clear">
	                	<?php //the_content(); ?>
						<div id="tabs">
							<ul>
								<li><a href="#tabs-1">PRIVATE</a></li>
								<li><a href="#tabs-2">GROUP</a></li>
								<li><a href="#tabs-3">PRIVATE</a></li>
								<li><a href="#tabs-4">GROUP</a></li>
							</ul>
							<div id="tabs-1">

								<?php if( get_field("disable_tab_1") != "Yes" ){ ?>
								<div class="container">
									<h3 class="text-center" style="margin-top: 30px;"><?php the_field("content_title"); ?></h3>
										<p class="text-center" style="margin-bottom: 10px;">
											<?php the_field("content_title_p"); ?>
										</p>
										<p class="text-center">
											<a class="btn-blue center-block btn-lg font-francoise" href="#calendar">SIGN UP</a>
										</p>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_1"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_2"); ?>
											</div>
										</div>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_3"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_4"); ?>
											</div>
										</div>
								</div>


								<div id="map-canvas"></div>
								<div class="container clearfix" style="margin-top:30px">
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width:53%;float:left;">
											<h4 class="text-green" style="font-size: 24px;text-align: right;padding-top: 10px;color: #333;"><?php the_field("location_title"); ?></h4>
										</div>
										<div style="width:45%;float:left;">
											<h4 class="text-green" style="text-align: left;box-sizing:border-box;padding-left:20px;line-height: 22px;">44 East 1st Street<br/>
											<span style="font-size:22px;">Ocean Isle Beach, NC</span></h4>
										</div>
									</div>
									<p class="text-center"><?php the_field("location_description"); ?></p>
									<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Covered Gazebo</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Showers</p>
										</div>
									</div>
									<div class="clearfix text-center row" style="margin-bottom: 20px;box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Free Parking Available</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Public Restrooms</p>
										</div>
									</div>
								</div>
								<div style="background-color: rgb(236, 235, 231);background-image: url('/wp-content/themes/inspire/images/fruute-special-cookie-cloud.jpg');background-position: center 0px;padding:30px 0px 25px 0px;background-repeat: repeat-x;">
									<div class="container clearfix">
										<h4 class="text-center text-green" style="color: #333;margin-top: 20px;">Overview</h4>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-top:30px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>Surfboard Included</p>
											</div>
											<div style="width:50%;float:left;">
												<p>One on One Instruction</p>
											</div>
										</div>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-bottom:20px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>11:30am - 1:00pm</p>
											</div>
											<div style="width:50%;float:left;">
												<p>All Ages</p>
											</div>
										</div>
										<div class="container" style="padding: 0px 100px;">
											<h4 style="font-size: 24px;color: #333;margin-top: 20px;margin-bottom:12px;"><?php the_field("cloud_container_title"); ?></h4>
											<p><?php the_field("cloud_container_content"); ?></p>
										</div>
										<?php 
											$cloud_img = get_field("cloud_container_image");
											if($cloud_img){
												echo "<img class='center-block' src='".$cloud_img["url"]."' />";
											}
										?>
									</div>
								</div>
								<div class="container clearfix">
									<h3 id="calendar" style="font-size:24px;color:#333;margin-top:30px;" class="text-center">INTERACTIVE BOOKING CALENDAR</h3>
									<div style="margin-left:3%;">
									<?php the_field("calendar_script"); ?>
									</div>
									<h3 style="font-size:24px;color:#333;margin: 20px 0px 0px 3%;"><?php the_field("policy_title"); ?></h3>
									<p style="margin-left:3%;;">
										<?php the_field("policy_content"); ?>  
									</p>
									<div class="clearfix text-center" style="margin: 0px auto 10px auto;width:590px;">
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a class="btn-white" style="padding:5px 67px;" href="mailto:carolinaschoolofsurf@gmail.com">EMAIL US</a>
										</div>
										<div class="text-muted font-tillium" style="width:33%;box-sizing:border-box;float:left;">
											HOW CAN WE HELP
										</div>
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a style="padding: 5px 50px;" class="btn-white" href="javascript:void(0)">(910)713-9283</a>
										</div>
									</div>
								</div>

								<?php } ?>

								<div class="container">
								<?php the_field("tab_1_private_lesson_content"); ?>
								</div>

							</div>
							<div id="tabs-2">

								<?php if( get_field("disable_tab_2") != "Yes" ){ ?>
								<div class="container">
									<h3 class="text-center" style="margin-top: 30px;"><?php the_field("content_title_(copy)"); ?></h3>
										<p class="text-center" style="margin-bottom: 10px;">
											<?php the_field("content_title_p_(copy)"); ?>
										</p>
										<p class="text-center">
											<a class="btn-blue center-block btn-lg font-francoise" href="#calendar2">SIGN UP</a>
										</p>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_1_(copy)"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_2_(copy)"); ?>
											</div>
										</div>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_3_(copy)"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_4_(copy)"); ?>
											</div>
										</div>
								</div>


								<div id="map-canvas2"></div>
								<div class="container clearfix" style="margin-top:30px">
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width:53%;float:left;">
											<h4 class="text-green" style="font-size: 24px;text-align: right;padding-top: 10px;color: #333;"><?php the_field("location_title_(copy)"); ?></h4>
										</div>
										<div style="width:45%;float:left;">
											<h4 class="text-green" style="text-align: left;box-sizing:border-box;padding-left:20px;line-height: 22px;">44 East 1st Street<br/>
											<span style="font-size:22px;">Ocean Isle Beach, NC</span></h4>
										</div>
									</div>
									<p class="text-center"><?php the_field("location_description_(copy)"); ?></p>
									<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Covered Gazebo</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Showers</p>
										</div>
									</div>
									<div class="clearfix text-center row" style="margin-bottom: 20px;box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Free Parking Available</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Public Restrooms</p>
										</div>
									</div>
								</div>
								<div style="background-color: rgb(236, 235, 231);background-image: url('/wp-content/themes/inspire/images/fruute-special-cookie-cloud.jpg');background-position: center 0px;padding:30px 0px 25px 0px;background-repeat: repeat-x;">
									<div class="container clearfix">
										<h4 class="text-center text-green" style="color: #333;margin-top: 20px;">Overview</h4>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-top:30px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>Surfboard Included</p>
											</div>
											<div style="width:50%;float:left;">
												<p>3:1 Surfer/Instructor Ratio</p>
											</div>
										</div>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-bottom:20px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>11:30am - 1:00pm</p>
											</div>
											<div style="width:50%;float:left;">
												<p>Ages 13+</p>
											</div>
										</div>
										<div class="container" style="padding: 0px 100px;">
											<h4 style="font-size: 24px;color: #333;margin-top: 20px;margin-bottom:12px;"><?php the_field("cloud_container_title_(copy)"); ?></h4>
											<p><?php the_field("cloud_container_content_(copy)"); ?></p>
										</div>
										<?php 
											$cloud_img = get_field("cloud_container_image_(copy)");
											if($cloud_img){
												echo "<img class='center-block' src='".$cloud_img["url"]."' />";
											}
										?>
									</div>
								</div>
								<div class="container clearfix">
									<h3 id="calendar2" style="font-size:24px;color:#333;margin-top:30px;" class="text-center">INTERACTIVE BOOKING CALENDAR</h3>
									<div style="margin-left:3%;">
									<?php the_field("calendar_script_(copy)"); ?>
									</div>
									<h3 style="font-size:24px;color:#333;margin: 20px 0px 0px 3%;"><?php the_field("policy_title_(copy)"); ?></h3>
									<p style="margin-left:3%;;">
										<?php the_field("policy_content_(copy)"); ?>  
									</p>
									<div class="clearfix text-center" style="margin: 0px auto 10px auto;width:590px;">
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a class="btn-white" style="padding:5px 67px;" href="mailto:carolinaschoolofsurf@gmail.com">EMAIL US</a>
										</div>
										<div class="text-muted font-tillium" style="width:33%;box-sizing:border-box;float:left;">
											HOW CAN WE HELP
										</div>
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a style="padding: 5px 50px;" class="btn-white" href="javascript:void(0)">(910)713-9283</a>
										</div>
									</div>
								</div>

								<?php } ?>

								<div class="container">
								<?php the_field("tab_1_group_lesson_content"); ?>
								</div>

							</div>
							<div id="tabs-3">

								<?php if( get_field("disable_tab_3") != "Yes" ){ ?>
								<div class="container">
									<h3 class="text-center" style="margin-top: 30px;"><?php the_field("content_title_(copy)_(copy)"); ?></h3>
										<p class="text-center" style="margin-bottom: 10px;">
											<?php the_field("content_title_p_(copy)_(copy)"); ?>
										</p>
										<p class="text-center">
											<a class="btn-blue center-block btn-lg font-francoise" href="#calendar3">SIGN UP</a>
										</p>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_1_(copy)_(copy)"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_2_(copy)_(copy)"); ?>
											</div>
										</div>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_3_(copy)_(copy)"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_4_(copy)_(copy)"); ?>
											</div>
										</div>
								</div>


								<div id="map-canvas3"></div>
								<div class="container clearfix" style="margin-top:30px">
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width:53%;float:left;">
											<h4 class="text-green" style="font-size: 24px;text-align: right;padding-top: 10px;color: #333;"><?php the_field("location_title_(copy)_(copy)"); ?></h4>
										</div>
										<div style="width:45%;float:left;">
											<h4 class="text-green" style="text-align: left;box-sizing:border-box;padding-left:20px;line-height: 22px;">118 Ocean Blvd E<br/>
											<span style="font-size:22px;">&nbsp;Holden Beach, NC</span></h4>
										</div>
									</div>
									<p class="text-center"><?php the_field("location_description_(copy)_(copy)"); ?></p>
									<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Covered Gazebo</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Showers</p>
										</div>
									</div>
									<div class="clearfix text-center row" style="margin-bottom: 20px;box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Free Parking Available</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Public Restrooms</p>
										</div>
									</div>
								</div>
								<div style="background-color: rgb(236, 235, 231);background-image: url('/wp-content/themes/inspire/images/fruute-special-cookie-cloud.jpg');background-position: center 0px;padding:30px 0px 25px 0px;background-repeat: repeat-x;">
									<div class="container clearfix">
										<h4 class="text-center text-green" style="color: #333;margin-top: 20px;">Overview</h4>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-top:30px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>Surfboard Included</p>
											</div>
											<div style="width:50%;float:left;">
												<p>One on One Instruction</p>
											</div>
										</div>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-bottom:20px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>11:30am - 1:00pm</p>
											</div>
											<div style="width:50%;float:left;">
												<p>All Ages</p>
											</div>
										</div>
										<div class="container" style="padding: 0px 100px;">
											<h4 style="font-size: 24px;color: #333;margin-top: 20px;margin-bottom:12px;"><?php the_field("cloud_container_title_(copy)_(copy)"); ?></h4>
											<p><?php the_field("cloud_container_content_(copy)_(copy)"); ?></p>
										</div>
										<?php 
											$cloud_img = get_field("cloud_container_image_(copy)_(copy)");
											if($cloud_img){
												echo "<img class='center-block' src='".$cloud_img["url"]."' />";
											}
										?>
									</div>
								</div>
								<div class="container clearfix">
									<h3 id="calendar3" style="font-size:24px;color:#333;margin-top:30px;" class="text-center">INTERACTIVE BOOKING CALENDAR</h3>
									<div style="margin-left:3%;">
									<?php the_field("calendar_script_(copy)_(copy)"); ?>
									</div>
									<h3 style="font-size:24px;color:#333;margin: 20px 0px 0px 3%;"><?php the_field("policy_title_(copy)_(copy)"); ?></h3>
									<p style="margin-left:3%;;">
										<?php the_field("policy_content_(copy)_(copy)"); ?>  
									</p>
									<div class="clearfix text-center" style="margin: 0px auto 10px auto;width:590px;">
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a class="btn-white" style="padding:5px 67px;" href="mailto:carolinaschoolofsurf@gmail.com">EMAIL US</a>
										</div>
										<div class="text-muted font-tillium" style="width:33%;box-sizing:border-box;float:left;">
											HOW CAN WE HELP
										</div>
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a style="padding: 5px 50px;" class="btn-white" href="javascript:void(0)">(910)713-9283</a>
										</div>
									</div>
								</div>

								<?php } ?>

								<div class="container">
								<?php the_field("tab_2_private_lesson_content"); ?>
								</div>

							</div>
							<div id="tabs-4">

								<?php if( get_field("disable_tab_4") != "Yes" ){ ?>
								<div class="container">
									<h3 class="text-center" style="margin-top: 30px;"><?php the_field("content_title_(copy)_(copy)_(copy)"); ?></h3>
										<p class="text-center" style="margin-bottom: 10px;">
											<?php the_field("content_title_p_(copy)_(copy)_(copy)"); ?>
										</p>
										<p class="text-center">
											<a class="btn-blue center-block btn-lg font-francoise" href="#calendar4">SIGN UP</a>
										</p>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_1_(copy)_(copy)_(copy)"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_2_(copy)_(copy)_(copy)"); ?>
											</div>
										</div>
										<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;padding: 0px 150px;">
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_3_(copy)_(copy)_(copy)"); ?>
											</div>
											<div style="width:50%;float:left;">
												<?php the_field("green_text_box_4_(copy)_(copy)_(copy)"); ?>
											</div>
										</div>
								</div>


								<div id="map-canvas4"></div>
								<div class="container clearfix" style="margin-top:30px">
									<div class="clearfix text-center" style="box-sizing:border-box;margin-bottom:30px;">
										<div style="width:53%;float:left;">
											<h4 class="text-green" style="font-size: 24px;text-align: right;padding-top: 10px;color: #333;"><?php the_field("location_title_(copy)_(copy)_(copy)"); ?></h4>
										</div>
										<div style="width:45%;float:left;">
											<h4 class="text-green" style="text-align: left;box-sizing:border-box;padding-left:20px;line-height: 22px;">118 Ocean Blvd E<br/>
											<span style="font-size:22px;">&nbsp;Holden Beach, NC</span></h4>
										</div>
									</div>
									<p class="text-center"><?php the_field("location_description_(copy)_(copy)_(copy)"); ?></p>
									<div class="clearfix text-center row" style="box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Covered Gazebo</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Showers</p>
										</div>
									</div>
									<div class="clearfix text-center row" style="margin-bottom: 20px;box-sizing:border-box;padding: 0px 200px;">
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Free Parking Available</p>
										</div>
										<div style="width:50%;float:left;">
											<p class="text-muted font-tillium">Public Restrooms</p>
										</div>
									</div>
								</div>
								<div style="background-color: rgb(236, 235, 231);background-image: url('/wp-content/themes/inspire/images/fruute-special-cookie-cloud.jpg');background-position: center 0px;padding:30px 0px 25px 0px;background-repeat: repeat-x;">
									<div class="container clearfix">
										<h4 class="text-center text-green" style="color: #333;margin-top: 20px;">Overview</h4>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-top:30px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>Surfboard Included</p>
											</div>
											<div style="width:50%;float:left;">
												<p>3:1 Surfer/Instructor Ratio</p>
											</div>
										</div>
										<div class="clearfix text-center row" style="box-sizing:border-box;margin-bottom:20px;padding: 0px 200px;">
											<div style="width:50%;float:left;">
												<p>11:30am - 1:00pm</p>
											</div>
											<div style="width:50%;float:left;">
												<p>Ages 13+</p>
											</div>
										</div>
										<div class="container" style="padding: 0px 100px;">
											<h4 style="font-size: 24px;color: #333;margin-top: 20px;margin-bottom:12px;"><?php the_field("cloud_container_title_(copy)_(copy)_(copy)"); ?></h4>
											<p><?php the_field("cloud_container_content_(copy)_(copy)_(copy)"); ?></p>
										</div>
										<?php 
											$cloud_img = get_field("cloud_container_image_(copy)_(copy)_(copy)");
											if($cloud_img){
												echo "<img class='center-block' src='".$cloud_img["url"]."' />";
											}
										?>
									</div>
								</div>
								<div class="container clearfix">
									<h3 id="calendar4" style="font-size:24px;color:#333;margin-top:30px;" class="text-center">INTERACTIVE BOOKING CALENDAR</h3>
									<div style="margin-left:3%;">
									<?php the_field("calendar_script_(copy)_(copy)_(copy)"); ?>
									</div>
									<h3 style="font-size:24px;color:#333;margin: 20px 0px 0px 3%;"><?php the_field("policy_title_(copy)_(copy)_(copy)"); ?></h3>
									<p style="margin-left:3%;;">
										<?php the_field("policy_content_(copy)_(copy)_(copy)"); ?>  
									</p>
									<div class="clearfix text-center" style="margin: 0px auto 10px auto;width:590px;">
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a class="btn-white" style="padding:5px 67px;" href="mailto:carolinaschoolofsurf@gmail.com">EMAIL US</a>
										</div>
										<div class="text-muted font-tillium" style="width:33%;box-sizing:border-box;float:left;">
											HOW CAN WE HELP
										</div>
										<div style="width:33%;box-sizing:border-box;float:left;">
											<a style="padding: 5px 50px;" class="btn-white" href="javascript:void(0)">(910)713-9283</a>
										</div>
									</div>
								</div>

								<?php } ?>

								<div class="container">
								<?php the_field("tab_2_group_lesson_content"); ?>
								</div>

							</div>
						</div>

	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                   
                   <?php $comm = get_option('woo_comments'); if ( 'open' == $post->comment_status && ($comm == "page" || $comm == "both") ) : ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->
		
	</div><!-- /#col-full -->
    </div><!-- /#content -->


<style type="text/css">
  #map-canvas,#map-canvas2,
  #map-canvas3,#map-canvas4{
    margin: 0px auto;
    padding: 0;
    height: 350px;
    width: 100%;
  }
  @media(max-width: 1200px){
  	#map-canvas,#map-canvas2,
  	#map-canvas3,#map-canvas4{
  		width: 97%;
  	}
  }
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
  // Enable the visual refresh
  google.maps.visualRefresh = true;
  var map;
  var map2;
  var map3;
  var map4;
  function initialize() {
    var mapOptions = {
      zoom: 15,
      center: new google.maps.LatLng(33.888252, -78.432686),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      scrollwheel: false
    };
    var mapOptions2 = {
      zoom: 15,
      center: new google.maps.LatLng(33.913814, -78.265631),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      scrollwheel: false
    };
    if( $("#map-canvas").length > 0 )
    	map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
    if( $("#map-canvas2").length > 0 )
    	map2 = new google.maps.Map(document.getElementById('map-canvas2'),mapOptions);
    if( $("#map-canvas3").length > 0 )
    	map3 = new google.maps.Map(document.getElementById('map-canvas3'),mapOptions2);
    if( $("#map-canvas4").length > 0 )
    	map4 = new google.maps.Map(document.getElementById('map-canvas4'),mapOptions2);
    //Loading Markers
    var position = new Array();
    var position2 = new Array();
    position[0] = new google.maps.LatLng(33.888252, -78.432686);
    position2[0] = new google.maps.LatLng(33.913814, -78.265631);
    loadMarkers(position,0);
    loadMarkers2(position,0);
    loadMarkers3(position2,0);
    loadMarkers4(position2,0);
    function loadMarkers(position,i){
      var marker = new google.maps.Marker({
        position: position[i],
        map: map,
        animation: google.maps.Animation.DROP,
        //icon: image
        //title: 'Uluru (Ayers Rock)'
      });
      marker.setTitle((i + 1).toString());
      //attachSecretMessage(marker, i);
      if (i <= position.length)
        return loadMarkers(position,i+1);
    }
    function loadMarkers2(position,i){
      var marker = new google.maps.Marker({
        position: position[i],
        map: map2,
        animation: google.maps.Animation.DROP,
        //icon: image
        //title: 'Uluru (Ayers Rock)'
      });
      marker.setTitle((i + 1).toString());
      //attachSecretMessage(marker, i);
      if (i <= position.length)
        return loadMarkers2(position,i+1);
    }
    function loadMarkers3(position,i){
      var marker = new google.maps.Marker({
        position: position[i],
        map: map3,
        animation: google.maps.Animation.DROP,
        //icon: image
        //title: 'Uluru (Ayers Rock)'
      });
      marker.setTitle((i + 1).toString());
      //attachSecretMessage(marker, i);
      if (i <= position.length)
        return loadMarkers3(position,i+1);
    }
    function loadMarkers4(position,i){
      var marker = new google.maps.Marker({
        position: position[i],
        map: map4,
        animation: google.maps.Animation.DROP,
        //icon: image
        //title: 'Uluru (Ayers Rock)'
      });
      marker.setTitle((i + 1).toString());
      //attachSecretMessage(marker, i);
      if (i <= position.length)
        return loadMarkers4(position,i+1);
    }
    function attachSecretMessage(marker, num) {
      var message = Array('<b>Carolina School of Surf</b>');
      infowindow = new google.maps.InfoWindow({
        //content: message[num],
        maxWidth: 200
      });
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(message[num]);
        infowindow.open(marker.get('map'), marker);
        //toggleBounce(marker);
      });
      google.maps.event.trigger(marker, 'click');
    }
    //End Loading Markers
  }
  //End Initialize
  //ClickToggle Animation
  function toggleBounce(marker) {
    if (marker.getAnimation() != null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
  }
  //End ClickToggle Animation
  google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
	$(function() {
		$( "#tabs" ).tabs({
			collapsible: true,
			active: 0,
			show: { 
				effect: "fade"
			}
		});
	});
	jQuery(document).ready(function(){
		jQuery("#ui-id-1,#ui-id-2").click(function(){
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab1").addClass("tab-bg");
			initialize();
		});
		jQuery(".tab1").click(function(){
			$("#tabs").tabs("option", "active", 0);
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab1").addClass("tab-bg");
			initialize();
		});
		jQuery("#ui-id-3,#ui-id-4").click(function(){
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab2").addClass("tab-bg");
			initialize();
		});
		jQuery(".tab2").click(function(){
			$("#tabs").tabs("option", "active", 2);
			jQuery(".tab").removeClass("tab-bg");
			jQuery(".tab2").addClass("tab-bg");
			initialize();
		});

		var hash = window.location.hash;

		if( hash == "#1" ){
			jQuery("#ui-id-1").trigger("click");
		}else if( hash == "#2" ){
			jQuery("#ui-id-2").trigger("click");
		}else if ( hash == "#3" ){
			jQuery("#ui-id-3").trigger("click");
		}else if( hash == "#4" ){
			jQuery("#ui-id-4").trigger("click");
		}

	});
	$('.page-surflesson a').smoothScroll();

</script>

<?php get_footer(); ?>