	<?php if ( woo_active_sidebar('footer-left') || woo_active_sidebar('footer-right' ) ) : ?>
	<div id="footer-widgets">
    	<div class="col-full">

            <div class="left block">
                <?php woo_sidebar('footer-left'); ?>    
            </div>
            <div class="right block">
                <?php woo_sidebar('footer-right'); ?>    
            </div>
            <div class="fix"></div>

		</div><!-- /.col-full  -->
	</div><!-- /#footer-widgets  -->
    <?php endif; ?>
    
	<div id="footer">
    	<div class="col-full">
            <?php $footer_menu = get_option('footer_menu'); ?>
            <?php echo $footer_menu; ?>
            <div>
                <strong class="footer_topic">Questions</strong>
                <?php $contact_form = get_option('contact_form'); ?>
                <?php echo do_shortcode($contact_form); ?>
            </div>
            <div class="container-copyicons">
                <p class="copyright">
                <?php $footer_copyright = get_option('footer_copyright'); ?>
                <?php echo $footer_copyright; ?>
                </p>
            </div>
            <div class="icons">
                <?php $footer_icons = get_option('footer_icons'); ?>
                <?php echo $footer_icons; ?>
            </div>
		</div><!-- /.col-full  -->
	</div><!-- /#footer  -->

</div><!-- /#wrapper -->
<?php wp_footer(); ?>
<?php woo_foot(); ?>
</body>
</html>